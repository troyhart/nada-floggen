This is a simple project intended to demonstrate the usage of nada-logback
and nada-slf4j in order to generate structured logging output. The logging 
output is based in part on the contents of the set of files found in the 
installed application's "input" directory (or the files passed as arguments
when the application is invoked) and other randomized input.

This application provides a gradle build script, which has many tasks that
can be executed on the project. Among the tasks is one that will "install"
the application into: `build/install/nada-floggen`.

Usage:

```
$ cd path/to/nada-floggen
$ ./gradlew installApp
$ cd build/install/nada-floggen
$ ./bin/nada-floggen
```

You can execute this script in the `bin` directory as many times as you as you
like and each time every `input` file will be read and each line in every file
will become the message of a discrete log with randomized log details.

Logging output is controlled by the `nada-floggen/src/main/resources/logback.xml`
Canned log input is source controlled in `nada-floggen/src/dist/input` and
logback runtime configuration properties for the application are found in
`nada-floggen/src/dist/AppRuntime.properties`.