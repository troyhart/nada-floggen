Execute the [nada-floggen](bin/nada-floggen) (or [nada-floggen.bat](bin/nada-floggen.bat))
script to generate logs with semi-random content. By default when the application
is executed it will read every line of every file in the input directory and
will echo each line to the logger. You can pass file references on the command
line to read a specific file rather than the contents of the input directory 
(use a fully qualified file path). 

NOTE: when you execute the script, execute from this directory, not the bin
directory. Example:

```
$ ./bin/nada-floggen
```

Or, to process the file foo.txt in my home directory I would do this:

```
$ ./bin/nada-floggen /home/thart/foo.txt
```