/**
 * Provides a set of arbitrary classes which can be executed from the
 * command line in order to generate (fake) logging activity.
 */
package com.nada.floggen;