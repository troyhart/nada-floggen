package com.nada.floggen;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.nada.slf4j.MdcAutoClosable;


/**
 * This executable class will read the contents of each file from the 
 * <code>input</code> directory. This is the default behavior and is 
 * overridden by passing command line arguments; each command line argument 
 * is take as discrete input file path. Regardless of the sources of input, 
 * each line from all sources is read and discretely logged through this
 * class's logger, along with other random contextual data.
 * 
 * @author troy.hart@gmail.com
 *
 */
public class FileContentLogger
{
  static final Logger LOG = LoggerFactory.getLogger(FileContentLogger.class);

  static final Map<Integer, String> PRINCIPALS = new HashMap<>();

  static final Map<Integer, String> RESOURCEURIS = new HashMap<>();

  static final Map<Integer, String> IPADDRPARTS = new HashMap<>();
  static {
    PRINCIPALS.put(0, "Bob");
    PRINCIPALS.put(1, "Joe");
    PRINCIPALS.put(2, "Jane");
    PRINCIPALS.put(3, "Mary");
    RESOURCEURIS.put(0, "/one/silly.action");
    RESOURCEURIS.put(1, "/two/another/resource");
    RESOURCEURIS.put(2, "/three/whatever");
    RESOURCEURIS.put(3, "/four/another/thing");
    IPADDRPARTS.put(0, "168");
    IPADDRPARTS.put(1, "172");
    IPADDRPARTS.put(2, "16");
    IPADDRPARTS.put(3, "15");
    IPADDRPARTS.put(4, "2");
    IPADDRPARTS.put(5, "24");
    IPADDRPARTS.put(6, "19");
    IPADDRPARTS.put(7, "215");
    IPADDRPARTS.put(8, "119");
    IPADDRPARTS.put(9, "117");
  }

  public static void main(String[] args)
  {
    System.out.println("Ready read input and produce logging...");
    new FileContentLogger().renderLogging(args);
    System.out.println("Done producing randomized logging output.");
  }

  private Map<String, Integer> sessionMap = new HashMap<>();

  private Map<String, String> clientIpMap = new HashMap<>();

  private Random random;

  private FileContentLogger()
  {
    random = new Random();
  }

  /**
   * @param args
   */
  private void renderLogging(String[] args)
  {
    args = resolveInputFilePaths(args);
    for (String filePath : args) {
      System.out.println("* filePath -> " + filePath);
      logFileContents(Paths.get(filePath));
    }
  }

  /**
   * @param args
   * @return
   */
  private String[] resolveInputFilePaths(String[] args)
  {
    if (args == null || args.length == 0) {
      ArrayList<String> paths = new ArrayList<>();
      Path input = Paths.get("input");
      for (File file : input.toFile().listFiles()) {
        paths.add(file.getAbsolutePath());
      }
      args = paths.toArray(new String[]{});
    }
    return args;
  }

  void logFileContents(Path file)
  {
    Charset charset = Charset.forName("US-ASCII");
    try (BufferedReader reader = Files.newBufferedReader(file, charset)) {
      String logMessage;
      while ((logMessage = reader.readLine()) != null) {
        try (MdcAutoClosable mdc = new MdcAutoClosable()) {
          String principal = resolvePrincipal();
          mdc.put("PRINCIPAL", principal);
          mdc.put("SESSION_TRACE", resolveSession(principal));
          mdc.put("CLIENT_IP", resolveClientIp(principal));
          mdc.put("REQUEST_TRACE", resolveTraceId());
          mdc.put("REQUEST_URI", resolveRequestURI());
          doLogging(logMessage);
        }
      }
    }
    catch (IOException x) {
      System.err.format("IOException: %s%n", x);
    }
  }

  /**
   * @param line
   */
  private void doLogging(String line)
  {
    switch (random.nextInt(10)) {
      case 0 :
        LOG.debug(line);
        break;
      case 1 :
        LOG.warn(line);
        break;
      case 2 :
        LOG.error(line, resolveException());
        break;
      default :
        LOG.info(line);
    }
  }

  private Exception resolveException()
  {
    return new RuntimeException("This is just a sample of an exception...");
  }

  /**
   * @return
   */
  private String resolvePrincipal()
  {
    return PRINCIPALS.get(random.nextInt(4));
  }

  /**
   * @return
   */
  private String resolveRequestURI()
  {
    return RESOURCEURIS.get(random.nextInt(4));
  }

  /**
   * @return
   */
  private int resolveTraceId()
  {
    return random.nextInt(10000000);
  }

  private String resolveIpAddress()
  {
    return String.format("%s.%s.%s.%s", IPADDRPARTS.get(random.nextInt(10)), IPADDRPARTS.get(random.nextInt(10)),
        IPADDRPARTS.get(random.nextInt(10)), IPADDRPARTS.get(random.nextInt(10)));
  }

  private Integer resolveSession(String principal)
  {
    Integer sessionId = sessionMap.get(principal);
    if (sessionId == null) {
      sessionMap.put(principal, sessionId = resolveTraceId());
    }
    return sessionId;
  }

  private String resolveClientIp(String principal)
  {
    String clientIp = clientIpMap.get(principal);
    if (clientIp == null) {
      clientIpMap.put(principal, clientIp = resolveIpAddress());
    }
    return clientIp;
  }
}
